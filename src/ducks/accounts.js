import { find, propEq } from 'ramda'
import Immutable from 'immutable'

// Actions
const LIST = 'sidecar/account/LIST'
const CREATE = 'sidecar/account/CREATE'
const UPDATE = 'sidecar/account/UPDATE'
const DELETE = 'sidecar/account/DELETE'
const EDITING = 'sidecar/account/EDITING'

const defaultState = Immutable.Map({
  loading: false,
  initialized: false,
  entries: [],
  editingAccount: null,
})

// Reducer
export default function reducer(
  state = defaultState,
  {
    type,
    payload,
    error,
  } = {},
) {
  switch (type) {
    case EDITING: {
      return state.set('editingAccount', payload)
    }
    case LIST: {
      if (error) return state.set('loading', false)
      if (!payload) return state.set('loading', true)
      const entries = payload.map(data => ({
        ...data,
        accountName: `${data.firstName} ${data.lastName}`,
      }))
      return state
        .set('loading', false)
        .set('initialized', true)
        .set('entries', entries)
    }
    case DELETE:
    case UPDATE:
    case CREATE: {
      if (error) return state.set('loading', false)
      if (!payload) return state.set('loading', true)
      const entries = payload.map(data => ({
        ...data,
        accountName: `${data.firstName} ${data.lastName}`,
      }))
      return state
        .set('loading', false)
        .set('entries', entries)
    }
    default:
      return state
  }
}

// Action Creators
const isProduction = process.env.NODE_ENV === 'production'
const API_ROOT = isProduction ? '//sidecar.lulalachen.info/api' : 'http://localhost:8080/api'

const fetchResolver = (actionType, dispatch) => promise =>
  promise.then((response) => {
    if (response.ok) {
      return response.json()
    } else {
      throw Error(response.json())
    }
  })
    .then(({ payload }) => dispatch({
      type: actionType,
      payload,
    }))
    .catch(({ message }) => dispatch({
      type: actionType,
      error: message,
    }))

export const listAccounts = () => (dispatch) => {
  dispatch({ type: LIST })
  const apiCall = fetch(`${API_ROOT}/account`)
  return fetchResolver(LIST, dispatch)(apiCall)
}

export const createAccount = data => (dispatch) => {
  dispatch({ type: CREATE })
  const apiCall = fetch(`${API_ROOT}/account`, {
    method: 'POST',
    headers: {
      'content-type': 'application/json',
    },
    body: JSON.stringify(data),
  })
  return fetchResolver(CREATE, dispatch)(apiCall)
}

export const updateAccount = (data, accountId) => (dispatch) => {
  dispatch({ type: UPDATE })
  const apiCall = fetch(`${API_ROOT}/account/${accountId}`, {
    method: 'PUT',
    headers: {
      'content-type': 'application/json',
    },
    body: JSON.stringify(data),
  })
  return fetchResolver(UPDATE, dispatch)(apiCall)
}

export const deleteAccount = () => (dispatch, getState) => {
  const { accounts } = getState()
  const { accountId } = accounts.get('editingAccount', {})
  dispatch({ type: DELETE })
  const apiCall = fetch(`${API_ROOT}/account/${accountId}`, { method: 'DELETE' })
  return fetchResolver(DELETE, dispatch)(apiCall)
}

export const updateAccountEditing = accountId => (dispatch, getState) => {
  const { accounts } = getState()
  const entries = accounts.get('entries')
  const editingAccount = find(propEq('accountId', accountId))(entries)
  dispatch({
    type: EDITING,
    payload: editingAccount,
  })
}
