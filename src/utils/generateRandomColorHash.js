import { map, pipe, split, join } from 'ramda'

const availableLetters = '0123456789ABCDEF'

const getRandomLetter = () => availableLetters[Math.floor(Math.random() * 16)]

const colorFormat = '#000000'

const generateRandomColorHash = () =>
  pipe(
    split(''),
    map((str) => {
      if (str === '0') {
        return getRandomLetter()
      } else {
        return str
      }
    }),
    join(''),
  )(colorFormat)

export default generateRandomColorHash
