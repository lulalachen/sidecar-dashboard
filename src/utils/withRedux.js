import React from 'react'
import { Provider } from 'react-redux'
import logger from './logger'
import configureStore from '../ducks/store'

let reduxStore = null

const initRedux = (initialState) => {
  logger('[withRedux] initialState')
  logger(JSON.stringify(initialState))

  reduxStore = configureStore(initialState)

  logger('[withRedux] store inited.')
  return reduxStore
}

const withRedux = BaseComp => (
  class extends React.Component {
    constructor(props) {
      super(props)
      this.store = initRedux(props)
    }
    componentDidUpdate() {
      const { dispatch } = this.props
      if (dispatch) {
        this.store.dispatch(dispatch)
      }
    }
    render() {
      return (
        <Provider store={this.store}>
          <BaseComp {...this.props} />
        </Provider>
      )
    }
  }
)

export default withRedux
