const R = require('ramda')

const getRandomDigit = () => Math.floor(Math.random() * 10)

const idFormat = '0000 0000 0000 0000'
const generateRandomId = () =>
  R.pipe(
    R.split(''),
    R.map((str) => {
      if (str === '0') {
        return getRandomDigit()
      } else {
        return str
      }
    }),
    R.join(''),
  )(idFormat)

module.exports = generateRandomId
