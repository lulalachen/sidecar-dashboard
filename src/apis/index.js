const R = require('ramda')
const D = require('date-fp')
const { Router } = require('express')
const generateRandomId = require('../utils/generateRandomId')

const router = Router()

let mockupData = require('../components/dashboard/__mockup__/accountDetail').default

router.get('/account', (req, res) =>
  res.send({ message: 'success', payload: mockupData }))

router.post('/account', (req, res) => {
  const payload = req.body
  const accountId = generateRandomId()
  mockupData.push({
    accountId,
    createOn: D.format('YYYY-MM-DDThh:mm:ss', new Date()),
    ...payload,
  })
  res.send({ message: 'success', payload: mockupData })
})

router.put('/account/:accountId', (req, res) => {
  const { accountId } = req.params
  const payload = req.body

  const index = R.findIndex(R.propEq('accountId', accountId))(mockupData)
  if (index >= 0) {
    mockupData[index] = Object.assign(mockupData[index], payload)
    res.send({ message: 'success', payload: mockupData })
  } else {
    res.status(404).send({ message: 'account not found' })
  }
})

router.delete('/account/:accountId', (req, res) => {
  const { accountId } = req.params
  const index = R.findIndex(R.propEq('accountId', accountId))(mockupData)
  if (index >= 0) {
    mockupData = R.remove(index, 1, mockupData)
    res.send({ message: 'success', payload: mockupData })
  } else {
    res.status(404).send({ message: 'account not found' })
  }
})

module.exports = router
