const accountDetail = [
  {
    accountId: '5500 1234 5678 1232',
    firstName: 'Reed',
    lastName: 'Richard',
    createOn: '2018-02-26T01:08:51.513Z',
    membership: 350,
    coverageLevel: 70,
    revenue: 2000,
  },
  {
    accountId: '1235 1234 2232 1232',
    firstName: 'Trader',
    lastName: 'Joe',
    createOn: '2018-02-26T01:08:51.513Z',
    membership: 350,
    coverageLevel: 70,
    revenue: 3000,
  },
  {
    accountId: '2231 1234 5678 1232',
    firstName: 'Robert',
    lastName: 'Downey',
    createOn: '2018-02-26T01:08:51.513Z',
    membership: 350,
    coverageLevel: 70,
    revenue: 1500,
  },
  {
    accountId: '2155 1234 5678 1232',
    firstName: 'Peter',
    lastName: 'Parker',
    createOn: '2018-02-26T01:08:51.513Z',
    membership: 350,
    coverageLevel: 70,
    revenue: 3500,
  },
  {
    accountId: '2132 1234 5678 1232',
    firstName: 'Bruce',
    lastName: 'Wayne',
    createOn: '2018-02-26T01:08:51.513Z',
    membership: 350,
    coverageLevel: 70,
    revenue: 2000,
  },
]

exports.default = accountDetail
