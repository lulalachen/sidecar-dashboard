import React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { compose } from 'recompose'
import { Icon, Modal, SubmissionError } from 'semantic-ui-react'
import { Field, reduxForm } from 'redux-form'
import { createAccount } from 'ducks/accounts'
import Button from 'components/common/Button'
import renderField from 'components/common/renderField'


const FormContentWrap = styled.div`
  display: flex;
  flex-direction: column;
  margin: auto;
  width: 500px;
  padding: 1.5em;
  justify-content: center;
`

const NameWrap = styled.div`
  display: flex;
`

const ErrorMessage = styled.div`
  color: palevioletred;
`

const enhance = compose(
  connect(() => ({}), { createAccount }),
  reduxForm({
    form: 'CreateAccount',
    onSubmit: (values, dispatch, props) =>
      props.createAccount(values).then(() => {
        props.handleModalClose()
      }).catch((err) => {
        throw new SubmissionError({ _error: err })
      }),
  }),
)

const AccountModalDimmer = props => (
  <Modal
    dimmer
    closeOnEscape
    open={props.isModalOpen}
    onClose={props.handleModalClose}
  >
    <Modal.Header>
      <Icon name='close' color='teal' onClick={props.handleModalClose} />
      <p>Add new account</p>
    </Modal.Header>
    <FormContentWrap>
      <NameWrap>
        <Field
          name='firstName'
          type='text'
          component={renderField}
          placeholder='First name'
          style={{ marginRight: '15px', width: '60%' }}
        />
        <Field
          name='lastName'
          type='text'
          component={renderField}
          placeholder='Last name'
          style={{ width: '60%' }}
        />
      </NameWrap>
      <Field
        name='membership'
        type='number'
        component={renderField}
        placeholder='Membership'
      />
      <Field
        name='coverageLevel'
        type='number'
        component={renderField}
        placeholder='Coverage level'
      />
      <Field
        name='revenue'
        type='number'
        component={renderField}
        placeholder='Revenue'
      />
      {props.error && <ErrorMessage>{props.error}</ErrorMessage>}
      <Button
        style={{ margin: 'auto' }}
        onClick={props.handleSubmit}
        type='submit'
      >
        Add
      </Button>
    </FormContentWrap>
  </Modal>
)

export default enhance(AccountModalDimmer)
