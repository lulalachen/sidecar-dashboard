import styled from 'styled-components'

export const Wrap = styled.div`
  top: 0;
  right: ${({ isDrawerOpen }) => (isDrawerOpen ? '0' : '-40vw')};
  position: fixed;
  height: 100vh;
  width: 40vw;
  background: #FFFFFF;
  box-shadow: -1px 0 3px 0 #AAAAAA;
  transition: 0.2s all ease-in-out;
`

export const HeaderWrap = styled.div`
  padding: 20px 30px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  p {
    font-size: 24px;
    font-weight: bold;
    margin: 0;
  }
`

export const ContentWrap = styled.div`
  display: flex;
  padding: 30px;
  flex-direction: column;
  overflow: scroll;
`

export const Hr = styled.hr`
  color: #AAAAAA;
  opacity: 0.5;
`

export const IconWrap = styled.div`
  display: flex;
`
