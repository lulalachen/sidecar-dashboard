import React from 'react'
import { pure } from 'recompose'
import { ResponsiveBar } from '@nivo/bar'
import styled from 'styled-components'
import generateRandomColorHash from 'utils/generateRandomColorHash'


const Wrap = styled.div`
  display: flex;
  flex-direction: column;
  margin: 30px auto;
  height: 300px;
`

const ChartTitle = styled.div`
  margin-left: 20px;
  font-size: 24px;
`

const DashboardChart = ({ accounts }) => (
  <Wrap>
    <ChartTitle>Revenue</ChartTitle>
    <ResponsiveBar
      data={accounts}
      indexBy='accountName'
      keys={['revenue']}
      colorBy={generateRandomColorHash}
      enableLabel={false}
      padding={0.3}
      motionStiffness={80}
      motionDamping={20}
      margin={{
        top: 30,
        left: 50,
        bottom: 30,
        right: 50,
      }}
    />
  </Wrap>
)

export default pure(DashboardChart)
