import React from 'react'
import { compose } from 'recompose'
import D from 'date-fp'
import { Icon, Table } from 'semantic-ui-react'

const parseDateFormat = compose(
  D.format('MMM DD, YYYY'),
  D.parse('YYYY-MM-DDThh:mm:ss'),
)

const TableHeader = () => (
  <Table.Header>
    <Table.Row>
      <Table.HeaderCell>ACCOUNT</Table.HeaderCell>
      <Table.HeaderCell>CREATE ON</Table.HeaderCell>
      <Table.HeaderCell>MEMBERSHIP</Table.HeaderCell>
      <Table.HeaderCell>COVERAGE LEVEL</Table.HeaderCell>
      <Table.HeaderCell>REVENUE</Table.HeaderCell>
      <Table.HeaderCell />
    </Table.Row>
  </Table.Header>
)

const AccountTable = ({
  accounts,
  handleEditDrawerClick,
}) => (
  <Table
    basic='very'
    textAlign='center'
    verticalAlign='top'
  >
    <TableHeader />

    <Table.Body>
      {
        accounts.map(entry => (
          <Table.Row key={`account-detail-table-${entry.accountId}`}>
            <Table.Cell>
              {`${entry.firstName} ${entry.lastName}`}
            </Table.Cell>
            <Table.Cell>
              {parseDateFormat(entry.createOn)}
            </Table.Cell>
            <Table.Cell>
              $ {entry.membership}
            </Table.Cell>
            <Table.Cell>
              {entry.coverageLevel} %
            </Table.Cell>
            <Table.Cell>
              {entry.revenue}
            </Table.Cell>
            <Table.Cell>
              <Icon
                id={entry.accountId}
                onClick={handleEditDrawerClick}
                name='pencil'
                color='teal'
                size='large'
                style={{ cursor: 'pointer' }}
              />
            </Table.Cell>
          </Table.Row>
        ))
      }
    </Table.Body>
  </Table>
)

export default AccountTable
