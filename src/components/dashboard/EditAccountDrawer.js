import React from 'react'
import D from 'date-fp'
import { connect } from 'react-redux'
import { compose, withHandlers, branch, renderNothing } from 'recompose'
import { Icon } from 'semantic-ui-react'
import { Field, reduxForm } from 'redux-form'
import { updateAccount, deleteAccount } from 'ducks/accounts'
import Button from 'components/common/Button'
import renderField from 'components/common/renderField'
import { Wrap, HeaderWrap, ContentWrap, Hr, IconWrap } from './EditAccountDrawerComponents'

const parseDateFormat = compose(
  D.format('M/DD/YY'),
  D.parse('YYYY-MM-DDThh:mm:ss'),
)

const enhance = compose(
  connect(
    ({ accounts }) => ({ initialValues: accounts.get('editingAccount') }),
    { deleteAccount, updateAccount },
  ),
  reduxForm({
    form: 'EditAccount',
    enableReinitialize: true,
    onSubmit: (values, dispatch, props) => {
      props.updateAccount(values, props.editingAccount.accountId)
      props.handleEditDrawerClose()
    },
  }),
  withHandlers({
    handleDeleteAccountClick: props => () => {
      props.deleteAccount()
      props.handleEditDrawerClose()
    },
  }),
  branch(
    ({ editingAccount }) => !editingAccount || editingAccount === null,
    renderNothing,
  ),
)

const EditAccountDrawer = ({
  isDrawerOpen,
  editingAccount,
  handleDeleteAccountClick,
  handleEditDrawerClose,
  handleSubmit,
}) => (
  <Wrap
    isDrawerOpen={isDrawerOpen}
  >
    <HeaderWrap>
      <p>Account {editingAccount.accountId}</p>
      <IconWrap>
        <Icon name='trash outline' color='teal' size='large' onClick={handleDeleteAccountClick} />
        <Icon name='close' color='teal' size='large' onClick={handleEditDrawerClose} />
      </IconWrap>
    </HeaderWrap>
    <Hr />
    <ContentWrap>
      <div>
        Created on {parseDateFormat(editingAccount.createOn)}
      </div>
      <Field
        name='firstName'
        type='text'
        component={renderField}
        placeholder='First name'
      />
      <Field
        name='lastName'
        type='text'
        component={renderField}
        placeholder='Last name'
      />
      <Field
        name='membership'
        type='number'
        component={renderField}
        placeholder='Membership'
      />
      <Field
        name='coverageLevel'
        type='number'
        component={renderField}
        placeholder='Coverage level'
      />
      <Field
        name='revenue'
        type='number'
        component={renderField}
        placeholder='Revenue'
      />
      <Button onClick={handleSubmit} type='submit'>Update</Button>
    </ContentWrap>
  </Wrap>
)

export default enhance(EditAccountDrawer)
