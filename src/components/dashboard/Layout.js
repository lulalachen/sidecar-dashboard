import React from 'react'
import styled from 'styled-components'
// import { Dimmer } from 'semantic-ui-react'
import LeftRail from 'components/dashboard/LeftRail'
import Spinner from 'components/common/Spinner'

const BodyWrap = styled.div`
  display: flex;
  height: 100%;
`

const MainSectionWrap = styled.div`
  width: 100%;
  padding: 30px 40px 30px 105px;
  h1 {
    margin: 0;
  }
`

const Dimmer = styled.div`
  position: fixed;
  z-index: 999;
  width: 100vw;
  height: 100vh;
  background: rgba(255,255,255,.85);
  display: ${({ active }) => (active ? 'flex' : 'none')};
  justify-content: center;
  align-items: center;
`

const LoadingDimmer = ({ active }) => (
  <Dimmer page active={active} >
    <Spinner />
  </Dimmer>
)

const DashboardLayout = ({
  children,
  loading,
  initialized = false,
}) => (
  <BodyWrap>
    <LoadingDimmer active={loading || !(initialized)} />
    <LeftRail />
    <MainSectionWrap>
      {children}
    </MainSectionWrap>
  </BodyWrap>
)

export default DashboardLayout
