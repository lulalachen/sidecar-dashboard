import React from 'react'
import styled from 'styled-components'

const Wrap = styled.div`
  width: 65px;
  height: 100%;
  position: fixed;
  background-color: #2C949A;

  img {
    width: 35px;
    height: 27px;
    margin: 19px 15px auto 15px;
  }
`

const LeftRail = () => (
  <Wrap>
    <img src='/static/logo.png' alt='SidecarLogo' />
  </Wrap>
)

export default LeftRail
