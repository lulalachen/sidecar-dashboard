import React from 'react'
import styled, { keyframes } from 'styled-components'

const spinning = keyframes`
  0% {
    transform: rotate(0);
    animation-timing-function: cubic-bezier(.55,.055,.675,.19);
  }
  50% {
    transform: rotate(180deg);
    animation-timing-function: cubic-bezier(.215,.61,.355,1);
  }
  100% {
    transform: rotate(360deg);
  }
`

const SpinningImage = styled.img`
  width: 50px;
  transform-origin: 17.5px 30.5px;
  animation: 1s ${spinning} infinite;
`
const Spinner = () => (
  <SpinningImage src='static/logo-colored.png' />
)

export default Spinner
