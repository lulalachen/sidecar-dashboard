import React from 'react'
import { isEmpty } from 'ramda'
import styled from 'styled-components'


const InputWrap = styled.span`
  position: relative;
  padding-top: 10px;
  width: ${({ fluid }) => (fluid ? '100%' : 'auto')};
`

const InputField = styled.input`
  color: #555555;
  border: 1px solid #AAAAAA;
  margin: 10px 0;
  padding: 17px;
  width: ${({ fluid }) => (fluid ? '100%' : 'auto')};

  :focus {
    border: 2px solid #64C5AF;
  }

  ::placeholder {
    color: #64C5AF;
  }

  :focus::placeholder {
    color: #FFFFFF;
  }

  :focus ~ .placeholder {
    display: block;
  }
`

const PlaceHolder = styled.span`
  position: absolute;
  left: 10px;
  top: 10px;
  background-color: #FFFFFF;
  color: #64C5AF;
  padding: 0 10px;
  display: ${({ isInputEmpty }) => (isInputEmpty ? 'none' : 'block')};
`

const Input = props => (
  <InputWrap fluid={props.fluid}>
    <InputField {...props} />
    <PlaceHolder
      className='placeholder'
      isInputEmpty={isEmpty(props.value)}
    >
      {props.placeholder}
    </PlaceHolder>
  </InputWrap>
)

export default Input
