import React from 'react'
import Input from './Input'

const renderField = ({
  id,
  input,
  type,
  placeholder,
  className,
}) => (
  <Input
    id={id}
    className={className}
    type={type}
    placeholder={placeholder}
    fluid
    {...input}
  />
)

export default renderField
