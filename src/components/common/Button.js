import styled from 'styled-components'


const Button = styled.button`
  color: #FFFFFF;
  background-color: #64C5AF;
  height: 50px;
  width: 200px;
  border: none;
  border-radius: 5px;
  margin-top: 5px;
  cursor: pointer;

  :hover, :active {
    opacity: 0.7;
  }
`

export default Button
