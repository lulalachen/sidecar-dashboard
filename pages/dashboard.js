import React from 'react'
import { connect } from 'react-redux'
import { compose, withState, withHandlers, lifecycle } from 'recompose'
import { Icon } from 'semantic-ui-react'
import withRedux from 'utils/withRedux'
import Layout from 'components/dashboard/Layout'
import DashboardChart from 'components/dashboard/DashboardChart'
import AccountTable from 'components/dashboard/AccountTable'
import AccountModalDimmer from 'components/dashboard/AccountModalDimmer'
import EditAccountDrawer from 'components/dashboard/EditAccountDrawer'
import Button from 'components/common/Button'

import { listAccounts, updateAccountEditing } from 'ducks/accounts'


const enhance = compose(
  withRedux,
  connect(
    ({ accounts }) => ({
      accounts: accounts.get('entries'),
      loading: accounts.get('loading'),
      initialized: accounts.get('initialized'),
      editingAccount: accounts.get('editingAccount'),
    }),
    ({ listAccounts, updateAccountEditing }),
  ),
  withState('isModalOpen', 'updateModalOpen', false),
  withState('isDrawerOpen', 'updateDrawerOpen', false),
  withHandlers({
    handleCreateAccountClick: ({ updateModalOpen }) => () => updateModalOpen(true),
    handleCreateAccountModalClose: ({ updateModalOpen }) => () => updateModalOpen(false),
    handleEditDrawerClick: props => (e) => {
      props.updateAccountEditing(e.target.id)
      props.updateDrawerOpen(true)
    },
    handleEditDrawerClose: ({ updateDrawerOpen }) => () => updateDrawerOpen(false),
  }),
  lifecycle({
    componentDidMount() {
      this.props.listAccounts()
    },
  }),
)

const Dashboard = ({
  accounts = [],
  isModalOpen,
  handleCreateAccountClick,
  handleCreateAccountModalClose,
  isDrawerOpen,
  handleEditDrawerClick,
  handleEditDrawerClose,
  editingAccount,
  loading,
  initialized,
}) => (
  <Layout loading={loading} initialized={initialized}>
    <h1>Accounts</h1>
    <DashboardChart accounts={accounts} loading={loading} />
    <Button
      onClick={handleCreateAccountClick}
      style={{ marginBottom: '30px', width: '120px' }}
    >
      <Icon name='plus' size='large' />
      New
    </Button>
    <AccountTable
      accounts={accounts}
      handleEditDrawerClick={handleEditDrawerClick}
    />
    <EditAccountDrawer
      isDrawerOpen={isDrawerOpen}
      handleEditDrawerClose={handleEditDrawerClose}
      editingAccount={editingAccount}
    />
    <AccountModalDimmer
      isModalOpen={isModalOpen}
      handleModalClose={handleCreateAccountModalClose}
    />
  </Layout>
)

export default enhance(Dashboard)
