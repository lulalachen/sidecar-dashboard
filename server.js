const express = require('express')
const bodyParser = require('body-parser')
const next = require('next')
const morgan = require('morgan')
const api = require('./src/apis')

const { NODE_ENV: env } = process.env

const nextApp = next({ dev: env !== 'production' })
const handler = nextApp.getRequestHandler()

nextApp.prepare().then(() => {
  const app = express()
  app.use(bodyParser.json())
  app.use(morgan('tiny'))
  app.use('/api', api)
  app.get(/\/((?!api).)*/, (req, res) => handler(req, res))

  /* eslint-disable no-console */
  const port = process.env.PORT || 8080
  app.listen(port, (err) => {
    if (err) {
      console.log(err)
    }
    console.log(`
  ⚡ 🚀  Web Server Start  🚀 ⚡

  ⚡  NODE_ENV = ${process.env.NODE_ENV}
  ⚡  Web Server runs on http://localhost:${port}
  `)
  })
}).catch((err) => {
  console.log(err.stack)
  process.exit(1)
})
