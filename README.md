# Account Dashboard

## Development
- `yarn install`
- `yarn run watch`

## Deployment
- `yarn install`
- `yarn build`
- `yarn run start:pm2`

## Author
Lulala Chen [Github](https://github.com/lulalachen/)

## Licence
[MIT](https://lulalachen.mit-license.org/)
