/* eslint-disable no-console, no-param-reassign */
module.exports = {
  distDir: './build',
  webpack: (config, { dev }) => {
    if (!dev) {
      config.plugins = config.plugins.filter(plugin => plugin.constructor.name !== 'UglifyJsPlugin')
    }
    return config
  },
}
